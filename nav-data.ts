import {INavBarData} from './helper';

export const NavbarData: INavBarData[]=[
    {
        routerlink:'item',
        icon:'',
        label:"Tableau général"
    },
    {
        routerlink:'TableGain',
        icon:'',
        label:"Tableau des gains"
    },
    {
        routerlink:'TableMise',
        icon:'',
        label:"Tableau des mises"
    },
    {
        routerlink:'administrateur',
        icon:'',
        label:"administrateur",
        items:[
            {
                routerlink:'admi/utilisateur',
                label:"Utilisateurs",
            },
            {
                routerlink:'admi/groupeDate',
                label:"Groupes et dates",
            },
            {
                routerlink:'admi/limiteTicket',
                label:"Limite ticket",
            },
            {
                routerlink:'admi/limiteGain',
                label:"Limite gain",
            },
        ]
    },
]
