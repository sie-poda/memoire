import { Component, OnInit, Output,EventEmitter, HostListener } from '@angular/core';
import {NavbarData} from './nav-data'
import {animate,keyframes,trigger,transition,style} from '@angular/animations';

interface SideNavToggle{
  screenwith:number;
  collapse:boolean
}
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  animations:[
    trigger('fadeInOut',[
      transition(':enter',[
        style({opacity:0}),
        animate('350ms',
        style({opacity:1})
        )
      ]),
      transition(':leave',[
        style({opacity:1}),
        animate('350ms',
        style({opacity:0 })
        )
      ])
    ]),
    trigger('rotate',[
      transition(':enter',[
        animate('1000ms')
      ])
    ])

    
  ]
})
export class SidebarComponent implements OnInit {


  @Output() onToggleSidenav: EventEmitter<SideNavToggle>=new EventEmitter()
  collapsed=true;
  screenwith=0
  //navData: any;
  navData= NavbarData;
  constructor() { }

  @HostListener('window:resize',['$event'])
  onResize(event:any){
    this.screenwith= window.innerWidth
    if(this.screenwith <= 768){
      this.collapsed=false
      this.onToggleSidenav.emit({collapse:this.collapsed,screenwith:this.screenwith})
    }
  }
  ngOnInit(): void {
    this.screenwith= window.innerWidth
  }

  toggleCollapse():void{
  this.collapsed=!this.collapsed;
  this.onToggleSidenav.emit({collapse:this.collapsed,screenwith:this.screenwith})
  }
  closeSidernav():void{
    this.collapsed=false
    this.onToggleSidenav.emit({collapse:this.collapsed,screenwith:this.screenwith})
  }
}
