export interface INavBarData{
    routerlink:string;
    icon?:string;
    label:string;
    expanded?:string;
    items?:INavBarData[];
}